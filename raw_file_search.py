"""An easy tool to search Listhub (or other raw) files for property or
agent data."""
import argparse
import logging
import gzip
import xml.etree.ElementTree as ET

OUTPUT_FILE = 'listingResult.xml'

## Functions
def parse_arguments():
    """Parse in user arguments"""
    parser = argparse.ArgumentParser(description="An easy tool to search "
                                                 "Listhub (or other raw) files "
                                                 "for property or agent data.")
    parser.add_argument('-d', '-date', type=str, nargs=1,
                        help='the date the listing was created (YYYY-MM-DD)')
    parser.add_argument('-s', '-source', type=str, nargs=1,
                        help='the listing source (listhub, corcoran)')
    parser.add_argument('-t', '-tag', type=str, nargs=1,
                        help='the tag to search for (ParticipantKey, OfficeKey, ListingKey, MlsId)')
    parser.add_argument('-v', '-value', type=str, nargs=1,
                        help='the value of the tag to search for')
    arguments = parser.parse_args()
    return arguments

def output_to_file(data, filename, mode, buf):
    """Generic write/append to file"""
    try:
        logging.info("Outputting " + data + " to file..")
        output_file = open(filename, mode, buf)
        output_file.write(data)
        output_file.close()
        logging.info("File output successful")
    except TypeError:
        logging.error("Could not output data to file")

# Transforming of Arguments
def read_directory_from_config():
    """read the desired directory path of .xml files from the scripts config.txt"""
    try:
        logging.debug("Reading config file")
        logging_file = open('config.txt', 'r')
        directory = logging_file.read()
        logging_file.close()
        logging.debug(directory + " read from config file")
        return directory
    except IOError:
        logging.error("No config.txt found. using '/data/provider' instead")
        return "./data/provider"

def transform_directory(source, date, directory):
    """Modify the directory path as required for different sources"""
    def corcoran():
        """the directory path to reference corcoran files"""
        return r"\JuwaiCorcoran.XML"
    def listhub():
        """the directory path to reference listhub files"""
        return r".xml.gz"
    def unitedprd():
        """the directory path to reference unitedprd files"""
        return r"\Juwai_Listings.xml"
    def windermere():
        """the directory path to reference windermere files"""
        return r"\WindermereJuwai.xml"

    source_dict = {"corcoran" : corcoran,
                   "listhub" : listhub,
                   "unitedprd" : unitedprd,
                   "windermere" : windermere,
                  }
    directory_path = directory + "\\" + source + "\\" + date + source_dict[source]()
    logging.debug(directory_path + " final directory path")
    return directory_path

def is_gzipped(source):
    """Modify the directory path as required for different sources"""
    def corcoran():
        """corcoran file are not gzipped"""
        return False
    def listhub():
        """listhub files are gzipped"""
        return True
    def unitedprd():
        """unitedprd files are not gzipped"""
        return False
    def windermere():
        """windermere files are not gzipped"""
        return False

    source_dict = {"corcoran" : corcoran,
                   "listhub" : listhub,
                   "unitedprd" : unitedprd,
                   "windermere" : windermere,
                  }
    result = source_dict[source]()
    logging.debug(source + " is gzipped: " + str(result))
    return result

def transform_date(invalid_date):
    """Insert -'s into the date argument as required for source's directory"""
    logging.info("Inputted date: " + invalid_date)
    year = invalid_date[0:len(invalid_date) / 2]
    second_half = invalid_date[len(invalid_date) / 2:]
    month = second_half[0:len(second_half) / 2]
    day = second_half[len(second_half) / 2:]
    invalid_date = year + '-' + month + '-' + day
    logging.info("Transformed date: " + invalid_date)
    return invalid_date

# XML parsing
def search_text(value, child, xml_arg):
    """Search the given tag's text for exact match or starts with (for %)"""
    result_string = ""
    if '%' in value:
        new_value = value.split('%', 1)
        if child.text.startswith(new_value[0]):
            result_string = ET.tostring(xml_arg, method="xml")
            logging.debug(child.text + " starts with " + new_value[0])
    else:
        if child.text == value:
            logging.debug(child.text + " is " + value)
            if xml_arg[0].text is None:
                result_string = ET.tostring(xml_arg[0], method="xml")
            else:
                result_string = ET.tostring(xml_arg, method="xml")
    return result_string

def search_xml_for_listing(xml_arg, tag, value):
    """Search the given xml for a tag that matches the value argument"""
    try:
        result = ""
        for child in xml_arg.iter():
            if tag in child.tag:
                result = search_text(value, child, xml_arg)
        return result
    except AttributeError:
        logging.error("Error reading xml data (most likely associated "
                      "with error reading file)")

def populate_tag_array(xml_file, args):
    """Populate an array with the first few tags - explained in find_obj_tag"""
    loop_counter = 0
    tags = []
    if is_gzipped(args.s[0]) is True:
        xml_file = gzip.open(xml_file)
    for event, curr_elem in ET.iterparse(xml_file,
                                         events=("start", "end")):
        if event == "start" and loop_counter < 3:
            tags.append(curr_elem.tag)
        else:
            break
        loop_counter = loop_counter + 1
    return tags

def find_obj_tag(xml_file, args):
    """loop through all the tags until an end tag matches one of the first
    few tags - this is the name of the object tag (usually <Listing> or <Propety>"""
    known_tags = populate_tag_array(xml_file, args)
    loop_counter = 0
    if is_gzipped(args.s[0]) is True:
        xml_file = gzip.open(xml_file)
    for event, curr_elem in ET.iterparse(xml_file,
                                         events=("start", "end")):
        # assumtion that the listing object will not be called <location>
        # or <Address> in any raw file
        if ((event == "end" and loop_counter >= 3 and curr_elem.tag != "Location") and
                ("Address" not in curr_elem.tag)):
            if curr_elem.tag in known_tags:
                result = curr_elem.tag
                logging.debug("xml object tag is " + result)
                break
        loop_counter = loop_counter + 1
    return result

def build_tree(xml_file, args):
    """Iterate through the file and search a single Listing element
    source - http://effbot.org/zone/element-iterparse.htm"""
    xml_obj_tag = find_obj_tag(xml_file, args)
    if is_gzipped(args.s[0]) is True:
        xml_file = gzip.open(xml_file)
    context = ET.iterparse(xml_file, events=("start", "end"))
    context = iter(context)
    event, root = context.next()
    # Search for </Listing> and then search it's contents
    for event, curr_elem in context:
        if event == "end" and curr_elem.tag == xml_obj_tag:
            listing_result = search_xml_for_listing(curr_elem, args.t[0], args.v[0])
            if len(listing_result) > 0:
                output_to_file(listing_result, OUTPUT_FILE, 'a', 0)
            root.clear()

def main():
    """Main body of the program"""
    # Create a log file
    logging.basicConfig(filename='debug.log', level=logging.DEBUG)
    output_to_file("<Listings>", OUTPUT_FILE, 'wb', 10)

    # Parse in arguments
    args = parse_arguments()
    valid_date = transform_date(args.d[0])

    # Search for & decompress .XML.gz file
    config_directory = read_directory_from_config()
    directory_of_xml_file = transform_directory(args.s[0], valid_date, config_directory)

    build_tree(directory_of_xml_file, args)

    # Required for valid xml
    output_to_file("</Listings>", OUTPUT_FILE, 'a', 0)


if __name__ == "__main__":
    main()
